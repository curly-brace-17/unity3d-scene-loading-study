using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoaderBehaviour : MonoBehaviour
{
    private AsyncOperation loading;
    private Boolean finished = false;
    
    private void Start()
    {
        loading = SceneManager.LoadSceneAsync("LoaderSceneBTest");
    }

    private void Update()
    {
        if (!finished)
            if (!loading.isDone)
            {
                Debug.Log($"Loading: {loading.progress}");
            }
            else
            {
                Debug.Log("Loaded");
                finished = true;                
            }
    }
}