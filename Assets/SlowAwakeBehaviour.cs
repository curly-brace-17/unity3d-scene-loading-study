using System;
using System.Threading;
using UnityEngine;

public class SlowAwakeBehaviour : MonoBehaviour
{
    // doesn't seem like this is the part of scene loading
    private void Awake()
    {
        Thread.Sleep(TimeSpan.FromSeconds(5));
    }
}
